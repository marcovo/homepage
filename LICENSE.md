# Partially open source

Copyright (c) Marco van Oort 2021

This code is available here as well as on https://marcovo.nl . You may not fork, copy or duplicate the code in any form, and may not use it as your own. You may have a look at it to get inspired. You may use the javascript code as reference for your own implementation of collision/gravity physics, but no guarantee is given on the physical correctness. Use at your own risk.
