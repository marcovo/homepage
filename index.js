const Simulation = function() {
    this.itemsContainer = null;

    this.items = [];
    this.itemDoublesX = [];
    this.itemDoublesY = [];
    this.itemDoublesXY = [];

    this.itemSizes = [];
    this.itemPositions = [];
    this.itemCenters = [];
    this.itemVelocities = [];
    this.itemWeights = [];

    this.screenWidth = null;
    this.screenHeight = null;

    this.lastFrame = null;
    this.fps_start = null;
    this.fps_frames = null;
    this.fps_cpu = null;

    this.initialSpeed = 100;

    this.gravitationActive = false;
    this.gravityItemId = null;
    this.gravityMousePosition = null;

    this.hoverItemId = null;
    this.hoverItemPosition = null;
    this.hoverItemVelocity = null;
};

Simulation.prototype.init = function() {
    this.itemsContainer = document.getElementById('items');
    this.screenWidth = this.itemsContainer.offsetWidth;
    this.screenHeight = this.itemsContainer.offsetHeight;

    let i = 0;
    for(const child of this.itemsContainer.children) {
        this.initItem(i, child);
        i++;
    }

    this.itemsContainer.addEventListener('mousedown', (evt) => {
        this.activateGravity(evt);
    });

    this.start();
};

Simulation.prototype.initItem = function(i, child, overrides) {
    overrides = overrides || {};

    child.setAttribute('data-item-id', i.toString());

    this.items.push(child);
    this.itemDoublesX.push(null);
    this.itemDoublesY.push(null);
    this.itemDoublesXY.push(null);

    this.itemSizes.push([child.offsetWidth, child.offsetHeight]);
    this.itemWeights.push(overrides.weight || (child.offsetWidth * child.offsetHeight));
    const position = overrides.position || [random(0, this.screenWidth), random(0, this.screenHeight)];
    this.itemPositions.push(position);
    this.itemCenters.push([
        position[0] + child.offsetWidth / 2,
        position[1] + child.offsetHeight / 2,
    ]);

    child.style.transform = 'translate(' + position[0] + 'px, ' + position[1] + 'px)';

    const angle = random(0, 2*Math.PI);
    this.itemVelocities.push(overrides.velocity || [Math.cos(angle) * this.initialSpeed, Math.sin(angle) * this.initialSpeed]);

    if(overrides.mouseover !== false) {
        child.addEventListener('mouseenter', () => {
            if(this.gravitationActive || this.hoverItemId !== null) {
                return;
            }

            this.hoverItemId = i;
            this.hoverItemPosition = this.itemPositions[i];
            this.hoverItemVelocity = this.itemVelocities[i];

            this.itemVelocities[i] = [0.0, 0.0];
        });

        child.addEventListener('mouseleave', () => {
            if(this.hoverItemId !== i) {
                return;
            }

            this.itemPositions[i] = this.hoverItemPosition;
            this.itemVelocities[i] = this.hoverItemVelocity;

            this.hoverItemId = null;
            this.hoverItemPosition = null;
            this.hoverItemVelocity = null;
        });
    }
};

Simulation.prototype.unsetItem = function(i) {
    if(this.itemDoublesX[i]) {
        this.itemsContainer.removeChild(this.itemDoublesX[i]);
    }
    if(this.itemDoublesY[i]) {
        this.itemsContainer.removeChild(this.itemDoublesY[i]);
    }
    if(this.itemDoublesXY[i]) {
        this.itemsContainer.removeChild(this.itemDoublesXY[i]);
    }

    this.items.splice(i, 1);
    this.itemDoublesX.splice(i, 1);
    this.itemDoublesY.splice(i, 1);
    this.itemDoublesXY.splice(i, 1);

    this.itemSizes.splice(i, 1);
    this.itemWeights.splice(i, 1);
    this.itemPositions.splice(i, 1);
    this.itemCenters.splice(i, 1);

    this.itemVelocities.splice(i, 1);
};

Simulation.prototype.start = function() {
    this.lastFrame = window.performance.now();
    window.requestAnimationFrame(() => {
        this.frame();
    });
};

Simulation.prototype.frame = function() {
    const thisFrame = window.performance.now();
    const dt = (thisFrame - this.lastFrame)/1000;

    this.step(dt);

    this.lastFrame = thisFrame;
    window.requestAnimationFrame(() => {
        this.frame();
    });

    if(this.fps_start === null) {
        this.fps_start = thisFrame;
        this.fps_frames = 1;
        this.fps_cpu = window.performance.now() - thisFrame;
    } else {
        if(thisFrame - this.fps_start > 1000) {
            const fps = this.fps_frames / (thisFrame - this.fps_start) * 1000;
            document.getElementById('fps').innerText = 'FPS: ' + fps.toFixed(1);

            const cpu = this.fps_cpu / (thisFrame - this.fps_start) * 100;
            document.getElementById('cpu').innerText = 'CPU: ' + cpu.toFixed(1) + '%';

            this.fps_start = thisFrame;
            this.fps_frames = 1;
            this.fps_cpu = window.performance.now() - thisFrame;
        } else {
            this.fps_frames++;
            this.fps_cpu += window.performance.now() - thisFrame;
        }
    }
};

Simulation.prototype.step = function(dt) {

    let deriv_x, deriv_v;

    // Determine collisions
    let firstCollisionDt = null, previousCollisionIndices = null;
    do {
        let firstCollisionIndices = null;
        firstCollisionDt = null;

        if(this.gravitationActive) {
            [deriv_x, deriv_v] = this.gravitationStep(dt);
        } else {
            [deriv_x, deriv_v] = this.linearStep();
        }

        for(let i = 0; i < this.items.length; i++) {
            for(let j = i+1; j < this.items.length; j++) {
                if(previousCollisionIndices !== null && i === previousCollisionIndices[0] && j === previousCollisionIndices[1]) {
                    // Sometimes rounding errors can cause a dt = +10^-16 being detected, while actually dt = 0
                    continue;
                }

                const x1 = [this.itemCenters[i][0], this.itemCenters[i][1]];
                const x2 = this.itemCenters[j];
                const v1 = deriv_x[i];
                const v2 = deriv_x[j];

                // Wrap the screen: make sure we are comparing the shortest wrapped distance
                if(x1[0] < x2[0] - this.screenWidth/2) {
                    x1[0] += this.screenWidth;
                } else if(x1[0] > x2[0] + this.screenWidth/2) {
                    x1[0] -= this.screenWidth;
                }

                if(x1[1] < x2[1] - this.screenHeight/2) {
                    x1[1] += this.screenHeight;
                } else if(x1[1] > x2[1] + this.screenHeight/2) {
                    x1[1] -= this.screenHeight;
                }

                // A collision happens when the distance equals the sum of radii.
                // (x1 - x2 + v1 * d - v2 * d)(x1 - x2 + v1 * d - v2 * d) === (diam1 + diam2) * (diam1 + diam2)

                // Rewrite collision equation into quadratic polynomial form in variable d
                // 0 === x1 * x1 - 2 * x1 * x2 + x2*x2 - (diam1 + diam2) * (diam1 + diam2)
                //    + (x1 * v1 - x1 * v2 - x2 * v1 + x2 * v2) * 2 * d
                //    + (v1 * v1 - 2 * v1 * v2 + v2 * v2) * d * d

                // Solve quadratic polynomial for collision time d (= dt1 V = dt2)
                const a = (v1[0] * v1[0] + v1[1] * v1[1]) - 2 * (v1[0] * v2[0] + v1[1] * v2[1]) + (v2[0] * v2[0] + v2[1] * v2[1]);
                const b = 2 * ( (x1[0] * v1[0] + x1[1] * v1[1]) - (x1[0] * v2[0] + x1[1] * v2[1]) - (x2[0] * v1[0] + x2[1] * v1[1]) + (x2[0] * v2[0] + x2[1] * v2[1]) );
                const c = (x1[0] * x1[0] + x1[1] * x1[1]) - 2 * (x1[0] * x2[0] + x1[1] * x2[1]) + (x2[0] * x2[0] + x2[1] * x2[1]) - (this.itemSizes[i][0] + this.itemSizes[j][0])*(this.itemSizes[i][0] + this.itemSizes[j][0])/4
                const D = b * b - 4 * a * c;

                if(D < 0) {
                    continue;
                }

                const dt1 = (-b - Math.sqrt(D)) / (2 * a);
                const dt2 = (-b + Math.sqrt(D)) / (2 * a);

                if(dt1 <= 0 || dt2 <= 0) {
                    // If both are negative, in the current neighbourhood they diverge, so no collision
                    // If just one is negative, they are inside each-other. This should not happen, but in case
                    // it does, we want them to escape that situation, so do not collide
                    continue;
                }
                const collisionDtMin = Math.min(dt1, dt2);

                if(collisionDtMin > dt) {
                    // No collision within current time step
                    continue;
                }

                // We have a collision within the current time step! Register it
                if(firstCollisionDt === null || collisionDtMin < firstCollisionDt) {
                    firstCollisionDt = collisionDtMin;
                    firstCollisionIndices = [i, j];
                }
            }
        }

        // If we have a collision within this time step, compute it
        if(firstCollisionDt !== null) {
            this.collide(firstCollisionIndices[0], firstCollisionIndices[1], deriv_x, firstCollisionDt);
            this.applyDelta(deriv_x, deriv_v, firstCollisionDt, firstCollisionIndices);

            this.resetFixatedItems();

            dt -= firstCollisionDt;
            previousCollisionIndices = firstCollisionIndices;
        }
    } while(firstCollisionDt !== null);

    // Make sure always make a step >>> Number.EPSILON to prevent rounding errors in the next frame
    this.applyDelta(deriv_x, deriv_v, Math.max(dt, 10**-9));

    this.resetFixatedItems();

    this.updateScreen();
};

Simulation.prototype.collide = function(i, j, deriv_x, dt) {
    // https://en.wikipedia.org/wiki/Elastic_collision
    // https://stackoverflow.com/questions/35211114/2d-elastic-ball-collision-physics

    // dt is the time to collision; compute new positions exactly at the time of collision
    this.itemPositions[i] = [
        this.itemPositions[i][0] + deriv_x[i][0] * dt,
        this.itemPositions[i][1] + deriv_x[i][1] * dt,
    ];
    const x1 = this.itemCenters[i] = [
        this.itemPositions[i][0] + this.itemSizes[i][0] / 2,
        this.itemPositions[i][1] + this.itemSizes[i][1] / 2,
    ];

    this.itemPositions[j] = [
        this.itemPositions[j][0] + deriv_x[j][0] * dt,
        this.itemPositions[j][1] + deriv_x[j][1] * dt,
    ];
    const x2 = this.itemCenters[j] = [
        this.itemPositions[j][0] + this.itemSizes[j][0] / 2,
        this.itemPositions[j][1] + this.itemSizes[j][1] / 2,
    ];

    // Wrap the screen: make sure we are comparing the shortest wrapped distance
    if(x1[0] < x2[0] - this.screenWidth/2) {
        x1[0] += this.screenWidth;
    } else if(x1[0] > x2[0] + this.screenWidth/2) {
        x1[0] -= this.screenWidth;
    }

    if(x1[1] < x2[1] - this.screenHeight/2) {
        x1[1] += this.screenHeight;
    } else if(x1[1] > x2[1] + this.screenHeight/2) {
        x1[1] -= this.screenHeight;
    }

    const v1 = deriv_x[i];
    const v2 = deriv_x[j];

    // Compute new velocities due to collision
    const mul = ((v1[0] - v2[0]) * (x1[0] - x2[0]) + (v1[1] - v2[1]) * (x1[1] - x2[1])) / ( (x1[0] - x2[0]) * (x1[0] - x2[0]) + (x1[1] - x2[1]) * (x1[1] - x2[1]) );
    let mul_i, mul_j;

    if(i === this.gravityItemId || j === this.gravityItemId || i === this.hoverItemId || j === this.hoverItemId) {
        // One item is fixed
        if(i === this.gravityItemId || i === this.hoverItemId) {
            mul_i = 0;
            mul_j = 2 * mul;
        } else {
            mul_i = 2 * mul;
            mul_j = 0;
        }
    } else {
        // Both items are moving
        mul_i = 2 * this.itemWeights[j] / (this.itemWeights[i] + this.itemWeights[j]) * mul;
        mul_j = 2 * this.itemWeights[i] / (this.itemWeights[i] + this.itemWeights[j]) * mul;
    }

    this.itemVelocities[i] = [
        v1[0] - mul_i * (x1[0] - x2[0]),
        v1[1] - mul_i * (x1[1] - x2[1]),
    ];

    this.itemVelocities[j] = [
        v2[0] - mul_j * (x2[0] - x1[0]),
        v2[1] - mul_j * (x2[1] - x1[1]),
    ];
};

Simulation.prototype.linearStep = function() {
    return [this.itemVelocities, null];
};

Simulation.prototype.applyDelta = function(deriv_x, deriv_v, dt, except) {
    if(except === undefined) {
        except = [];
    }

    // Apply derivative values
    for(const child of this.items) {
        const id = +(child.getAttribute('data-item-id'));

        if(except.indexOf(+id) > -1) {
            continue;
        }

        // Compute new position
        const newPosition = [
            this.itemPositions[id][0] + deriv_x[id][0] * dt,
            this.itemPositions[id][1] + deriv_x[id][1] * dt,
        ];

        if(deriv_v !== null) {
            this.itemVelocities[id] = [
                this.itemVelocities[id][0] + deriv_v[id][0] * dt,
                this.itemVelocities[id][1] + deriv_v[id][1] * dt,
            ];
        }

        const newCenter = [
            newPosition[0] + this.itemSizes[id][0] / 2,
            newPosition[1] + this.itemSizes[id][1] / 2,
        ];

        // Make sure the 'real' item always has a center within the screen boundaries (convention)
        if(newCenter[0] < 0) {
            newPosition[0] += this.screenWidth;
            newCenter[0] += this.screenWidth;
        } else if(newCenter[0] > this.screenWidth) {
            newPosition[0] -= this.screenWidth;
            newCenter[0] -= this.screenWidth;
        }

        if(newCenter[1] < 0) {
            newPosition[1] += this.screenHeight;
            newCenter[1] += this.screenHeight;
        } else if(newCenter[1] > this.screenHeight) {
            newPosition[1] -= this.screenHeight;
            newCenter[1] -= this.screenHeight;
        }

        // Update position
        this.itemPositions[id] = newPosition;
        this.itemCenters[id] = newCenter;
    }
};

Simulation.prototype.updateScreen = function() {
    // Update screen
    for(const child of this.items) {
        const id = +(child.getAttribute('data-item-id'));
        const newPosition = this.itemPositions[id];

        child.style.transform = 'translate(' + newPosition[0] + 'px, ' + newPosition[1] + 'px)';

        // Update clones; up to 4 required in case an item moves through a corner
        // We create clones as required and also remove them if they are not needed any more
        let clonePositionX = null, clonePositionY = null;

        if(newPosition[0] < 0 || newPosition[0] + this.itemSizes[id][0] > this.screenWidth) {
            if(this.itemDoublesX[id] === null) {
                this.itemDoublesX[id] = child.cloneNode(true);
                this.itemsContainer.appendChild(this.itemDoublesX[id]);
            }

            clonePositionX = [newPosition[0], newPosition[1]];
            if(clonePositionX[0] < 0) {
                clonePositionX[0] += this.screenWidth;
            } else if(newPosition[0] + this.itemSizes[id][0] > this.screenWidth) {
                clonePositionX[0] -= this.screenWidth;
            }

            this.itemDoublesX[id].style.transform = 'translate(' + clonePositionX[0] + 'px, ' + clonePositionX[1] + 'px)';

        } else {
            if(this.itemDoublesX[id] !== null) {
                this.itemsContainer.removeChild(this.itemDoublesX[id]);
                this.itemDoublesX[id] = null;
            }
        }

        if(newPosition[1] < 0 || newPosition[1] + this.itemSizes[id][1] > this.screenHeight) {
            if(this.itemDoublesY[id] === null) {
                this.itemDoublesY[id] = child.cloneNode(true);
                this.itemsContainer.appendChild(this.itemDoublesY[id]);
            }

            clonePositionY = [newPosition[0], newPosition[1]];
            if(clonePositionY[1] < 0) {
                clonePositionY[1] += this.screenHeight;
            } else if(newPosition[1] + this.itemSizes[id][1] > this.screenHeight) {
                clonePositionY[1] -= this.screenHeight;
            }

            this.itemDoublesY[id].style.transform = 'translate(' + clonePositionY[0] + 'px, ' + clonePositionY[1] + 'px)';

        } else {
            if(this.itemDoublesY[id] !== null) {
                this.itemsContainer.removeChild(this.itemDoublesY[id]);
                this.itemDoublesY[id] = null;
            }
        }

        if(this.itemDoublesX[id] !== null && this.itemDoublesY[id] !== null) {
            if(this.itemDoublesXY[id] === null) {
                this.itemDoublesXY[id] = child.cloneNode(true);
                this.itemsContainer.appendChild(this.itemDoublesXY[id]);
            }

            const clonePositionXY = [
                clonePositionX[0],
                clonePositionY[1],
            ];

            this.itemDoublesXY[id].style.transform = 'translate(' + clonePositionXY[0] + 'px, ' + clonePositionXY[1] + 'px)';

        } else {
            if(this.itemDoublesXY[id] !== null) {
                this.itemsContainer.removeChild(this.itemDoublesXY[id]);
                this.itemDoublesXY[id] = null;
            }
        }
    }
};

Simulation.prototype.gravitationStep = function(dt) {

    // https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods
    // http://www.vuduc.com/teaching/cse8803-pna-sp08/slides/cse8803-pna-sp08-20.pdf

    // k1
    const dx_k1 = this.itemVelocities;
    const dv_k1 = this.computeAccelerations(this.itemCenters);

    const x_k1 = [], v_k1 = [];
    for(let i = 0; i < this.items.length; i++) {
        x_k1[i] = [
            this.itemCenters[i][0] + dx_k1[i][0] * dt / 2,
            this.itemCenters[i][1] + dx_k1[i][1] * dt / 2,
        ];

        v_k1[i] = [
            this.itemVelocities[i][0] + dv_k1[i][0] * dt / 2,
            this.itemVelocities[i][1] + dv_k1[i][1] * dt / 2,
        ];
    }

    // k2
    const dx_k2 = v_k1;
    const dv_k2 = this.computeAccelerations(x_k1);

    const x_k2 = [], v_k2 = [];
    for(let i = 0; i < this.items.length; i++) {
        x_k2[i] = [
            this.itemCenters[i][0] + dx_k2[i][0] * dt / 2,
            this.itemCenters[i][1] + dx_k2[i][1] * dt / 2,
        ];

        v_k2[i] = [
            this.itemVelocities[i][0] + dv_k2[i][0] * dt / 2,
            this.itemVelocities[i][1] + dv_k2[i][1] * dt / 2,
        ];
    }

    // k3
    const dx_k3 = v_k2;
    const dv_k3 = this.computeAccelerations(x_k2);

    const x_k3 = [], v_k3 = [];
    for(let i = 0; i < this.items.length; i++) {
        x_k3[i] = [
            this.itemCenters[i][0] + dx_k3[i][0] * dt,
            this.itemCenters[i][1] + dx_k3[i][1] * dt,
        ];

        v_k3[i] = [
            this.itemVelocities[i][0] + dv_k3[i][0] * dt,
            this.itemVelocities[i][1] + dv_k3[i][1] * dt,
        ];
    }

    // k4
    const dx_k4 = v_k3;
    const dv_k4 = this.computeAccelerations(x_k3);

    const deriv_x = [];
    const deriv_v = [];

    // Compute final derivative values
    for(const child of this.items) {
        const id = +(child.getAttribute('data-item-id'));

        deriv_x[id] = [
            (dx_k1[id][0] + 2 * dx_k2[id][0] + 2 * dx_k3[id][0] + dx_k4[id][0]) / 6,
            (dx_k1[id][1] + 2 * dx_k2[id][1] + 2 * dx_k3[id][1] + dx_k4[id][1]) / 6,
        ];

        deriv_v[id] = [
            (dv_k1[id][0] + 2 * dv_k2[id][0] + 2 * dv_k3[id][0] + dv_k4[id][0]) / 6,
            (dv_k1[id][1] + 2 * dv_k2[id][1] + 2 * dv_k3[id][1] + dv_k4[id][1]) / 6,
        ];
    }

    return [deriv_x, deriv_v];
};

Simulation.prototype.computeAccelerations = function(itemCenters) {
    const accelerations = [];
    for(let i = 0; i < this.items.length; i++) {
        accelerations[i] = [0.0, 0.0];
    }

    for(let i = 0; i < this.items.length; i++) {
        for(let j = i+1; j < this.items.length; j++) {
            const x1 = [
                itemCenters[i][0],
                itemCenters[i][1],
            ];

            const x2 = [
                itemCenters[j][0],
                itemCenters[j][1],
            ];

            // Wrap the screen: make sure we are comparing the shortest wrapped distance
            if(x1[0] < x2[0] - this.screenWidth/2) {
                x1[0] += this.screenWidth;
            } else if(x1[0] > x2[0] + this.screenWidth/2) {
                x1[0] -= this.screenWidth;
            }

            if(x1[1] < x2[1] - this.screenHeight/2) {
                x1[1] += this.screenHeight;
            } else if(x1[1] > x2[1] + this.screenHeight/2) {
                x1[1] -= this.screenHeight;
            }

            // Force vector towards x1
            const diff = [
                x1[0] - x2[0],
                x1[1] - x2[1],
            ];
            const distance = Math.sqrt(diff[0]**2 + diff[1]**2);

            // Make sure no forces are contributed if two elements are inadvertently
            // within each other, to prevent singularities
            if(distance > (this.itemSizes[i][0] + this.itemSizes[j][0]) / 2) {
                const distance3 = distance ** 3;

                accelerations[i][0] += this.itemWeights[j] * diff[0] / distance3;
                accelerations[i][1] += this.itemWeights[j] * diff[1] / distance3;

                accelerations[j][0] += -this.itemWeights[i] * diff[0] / distance3;
                accelerations[j][1] += -this.itemWeights[i] * diff[1] / distance3;
            }
        }
    }

    const G = 5 * 10**1; //6.67408 * 10**-11;

    for(let i = 0; i < this.items.length; i++) {
        accelerations[i][0] *= -G;
        accelerations[i][1] *= -G;
    }

    return accelerations;
};

Simulation.prototype.activateGravity = function(evt) {
    if(evt.target.getAttribute('id') !== 'items') {
        // Don't trigger when clicking on items
        return;
    }

    this.gravitationActive = true;
    this.gravityMousePosition = [evt.clientX, evt.clientY];

    const deactivateGravity = () => {
        this.gravitationActive = false;

        this.itemsContainer.removeEventListener('mousemove', mouseMove);
        this.itemsContainer.removeEventListener('mouseup', deactivateGravity);
        this.itemsContainer.removeEventListener('mouseleave', deactivateGravity);

        this.unsetItem(this.gravityItemId);
        this.itemsContainer.removeChild(gravityItem);

        this.gravityItemId = null;
        this.gravityMousePosition = null;
    };

    const mouseMove = (evt) => {
        this.gravityMousePosition = [evt.clientX, evt.clientY];
    };

    this.itemsContainer.addEventListener('mousemove', mouseMove);
    this.itemsContainer.addEventListener('mouseup', deactivateGravity);
    this.itemsContainer.addEventListener('mouseleave', deactivateGravity);

    const gravityItem = document.createElement('div');
    gravityItem.setAttribute('class', 'item');
    gravityItem.style.width = '20px';
    gravityItem.style.height = '20px';

    let weight = 0;
    for(let i = 0; i < this.items.length; i++) {
        weight += this.itemWeights[i];
    }

    this.itemsContainer.appendChild(gravityItem);
    this.gravityItemId = this.items.length;
    this.initItem(this.gravityItemId, gravityItem, {
        weight: weight,
        position: [this.gravityMousePosition[0] - 10, this.gravityMousePosition[1] - 10],
        velocity: [0, 0],
        mouseover: false,
    });
};

Simulation.prototype.resetFixatedItems = function() {
    if(this.gravitationActive) {
        this.itemPositions[this.gravityItemId] = [this.gravityMousePosition[0] - 10, this.gravityMousePosition[1] - 10];
        this.itemVelocities[this.gravityItemId] = [0, 0];
    }

    if(this.hoverItemId !== null) {
        this.itemPositions[this.hoverItemId] = [this.hoverItemPosition[0], this.hoverItemPosition[1]];
        this.itemVelocities[this.hoverItemId] = [0, 0];
    }
};

let simulation;
let doAnimate;
window.addEventListener('load', function() {
    doAnimate = window.innerWidth >= 1000;

    if(doAnimate) {
        simulation = new Simulation();
        simulation.init();
    }

    generateBackground();

    if(doAnimate) {
        document.getElementById('rotateCheckbox').addEventListener('change', (evt) => {
            const background = document.getElementById('background');

            if(document.getElementById('rotateCheckbox').checked) {
                background.setAttribute('class', background.getAttribute('class') + ' background-rotating');
            } else {
                background.setAttribute('class', background.getAttribute('class').replace(/background-rotating/g, '').trim());
            }
        });
    }
});

function random(min, max) {
    return min + Math.random() * (max - min);
}

function generateBackground() {
    const canvas = document.createElement('canvas');
    canvas.width = Math.floor(window.innerWidth / 4);
    canvas.height = Math.floor(window.innerHeight / 4);
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const starTemplates = [
        {
            chance: 200,
            color: [1.0, 1.0, 1.0],
            grid: [
                [1.0]
            ]
        },
        {
            chance: 200,
            color: [1.0, 1.0, 1.0],
            grid: [
                [0.25, 0.25],
                [0.25, 0.25],
            ]
        },
        {
            chance: 20,
            color: [1.0, 1.0, 1.0],
            grid: [
                [0.25, 0.50, 0.50, 0.25],
                [0.50, 1.00, 1.00, 0.50],
                [0.50, 1.00, 1.00, 0.50],
                [0.25, 0.50, 0.50, 0.25],
            ]
        },
        {
            chance: 10,
            color: [1.0, 1.0, 0.7],
            grid: [
                [0.50, 0.25],
                [0.25, 0.50],
            ]
        },
        {
            chance: 10,
            color: [0.7, 0.7, 1.0],
            grid: [
                [0.25, 0.50],
                [0.50, 0.25],
            ]
        },
        {
            chance: 1,
            color: [1.0, 1.0, 1.0],
            grid: [
                [0.25, 0.50, 0.75, 0.50, 0.25],
                [0.50, 1.00, 1.00, 1.00, 0.50],
                [0.75, 1.00, 1.00, 1.00, 0.75],
                [0.50, 1.00, 1.00, 1.00, 0.50],
                [0.25, 0.50, 0.75, 0.50, 0.25],
            ]
        },
    ];

    let totalChance = 0;
    for(let i = 0; i < starTemplates.length; i++) {
        totalChance += starTemplates[i].chance;
    }

    const numStars = Math.floor(canvas.width * canvas.height / 400);
    for(let i = 0; i < numStars; i++) {
        let starTemplate;
        let choice = Math.floor(random(0, totalChance));
        for(starTemplate of starTemplates) {
            if(choice < starTemplate.chance) {
                break;
            } else {
                choice -= starTemplate.chance;
            }
        }

        const position = [random(0, canvas.width), random(0, canvas.height)];

        for(let y = 0; y < starTemplate.grid.length; y++) {
            for(let x = 0; x < starTemplate.grid[y].length; x++) {
                const color = [
                    Math.floor(starTemplate.grid[y][x] * starTemplate.color[0] * 255),
                    Math.floor(starTemplate.grid[y][x] * starTemplate.color[1] * 255),
                    Math.floor(starTemplate.grid[y][x] * starTemplate.color[2] * 255),
                ]
                ctx.fillStyle = 'rgb(' + color.join(',') + ')';
                ctx.fillRect(
                    (position[0] + x) % canvas.width,
                    (position[1] + y) % canvas.height,
                    1,
                    1
                );
            }
        }
    }

    const dataUrl = canvas.toDataURL('image/png');
    const background = document.getElementById(doAnimate ? 'background' : 'items');
    background.style.backgroundImage = 'url("' + dataUrl + '")';

    if(doAnimate) {
        const size = Math.max(window.innerHeight, window.innerWidth);
        let left = random(0, size);
        let top = random(0, size);
        // Max radius = sqrt(2) * (1.125 + 0.25) = 1.944 < 2, so fits within 400%x400% (diameter) box
        if(left < size/2) {
            left = -size * 0.125 + 0.25 * left;
        } else {
            left = size * 1.125 + 0.25 * left;
        }

        if(top < size/2) {
            top = -size * 0.125 + 0.25 * top;
        } else {
            top = size * 1.125 + 0.25 * top;
        }

        background.style.left = (-2 * size + left) + 'px';
        background.style.top = (-2 * size + top) + 'px';
    }
}
